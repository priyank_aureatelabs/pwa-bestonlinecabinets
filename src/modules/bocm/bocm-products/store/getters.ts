import BocmproductState from '../types/BocmproductState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<BocmproductState, any> = {
  getCategoryProductList: (state) => state.products
}
