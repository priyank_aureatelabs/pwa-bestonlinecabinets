import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { BocmCategoryModuleStore } from './store/index';

export const BocmCategoryModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('bocmCategory', BocmCategoryModuleStore)
};
