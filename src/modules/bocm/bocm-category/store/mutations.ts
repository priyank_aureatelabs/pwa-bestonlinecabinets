import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.FETCH_CATEGORY_FILTERS] (state, filters) {
    state.filters = filters || []
  },
  [types.SET_BOCM_SUB_CATEGORY] (state, category) {
    state.subCategory = category || []
  }
}
