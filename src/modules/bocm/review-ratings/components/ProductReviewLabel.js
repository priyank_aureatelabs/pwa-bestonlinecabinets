export const ProductReviewLabel = {
  props: {
    product: {
      type: Object,
      required: true,
      default: () => ({})
    }
  },
  methods: {
    scrollToOpenReviewForm () {
      const headerOffset = document.querySelector('.header').clientHeight;
      const offsetTop = document.querySelector('.product-rr-wrap').offsetTop;
      const offset = offsetTop - headerOffset;
      scroll({
        top: offset,
        behavior: 'smooth'
      });
    }
  }
}
