import AssemblyState from '../types/AssemblyState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<AssemblyState, any> = {
  getAssemblyList: (state) => state.items
}
