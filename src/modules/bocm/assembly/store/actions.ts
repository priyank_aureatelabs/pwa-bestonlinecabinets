import { ActionTree } from 'vuex';
import AssemblyState from '../types/AssemblyState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import { SearchQuery } from 'storefront-query-builder'

const AssemblyEntityName = 'flatpanel';
const actions: ActionTree<AssemblyState, any> = {
  /**
   * Retrieve groups
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */
  list (
    context, {
      skipCache = false
    }
  ) {
    let query = new SearchQuery();
    if (
      skipCache ||
      !context.state.items ||
      context.state.items.length === 0
    ) {
      return quickSearchByQuery({
        query,
        entityType: AssemblyEntityName
      })
        .then(resp => {
          context.commit(types.FETCH_ITEMS, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'Assembly Instructions')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.items;
        resolve(resp);
      });
    }
  }
};
export default actions;
