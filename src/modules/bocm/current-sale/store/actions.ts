import { ActionTree } from 'vuex';
import CurrentSaleState from '../types/CurrentSaleState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import { SearchQuery } from 'storefront-query-builder'

const alCurrentSaleEntityName = 'current_sale'
const actions: ActionTree<CurrentSaleState, any> = {
  /**
   * Retrieve groups
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list (context, { filterValues = null, filterField = 'push_live', excludeFields = null, includeFields = null, skipCache = false }) {
    let query = new SearchQuery();
    if (filterValues) {
      query = query.applyFilter({
        key: filterField,
        value: { eq: filterValues }
      });
    }
    if (skipCache || !context.state.groups || context.state.groups.length === 0) {
      return quickSearchByQuery({
        query,
        entityType: alCurrentSaleEntityName,
        excludeFields,
        includeFields
      })
        .then(resp => {
          context.commit(types.FETCH_GROUPS, resp.items[0]);
          return resp.items[0];
        })
        .catch(err => {
          Logger.error(err, 'groups')();
        });
    }
    return context.state.groups;
  }
};
export default actions;
