import { ActionTree } from 'vuex';
import ContactState from '../types/ContactState'
import rootStore from '@vue-storefront/core/store'
import config from 'config'
import i18n from '@vue-storefront/i18n'
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers';

const actions: ActionTree<ContactState, any> = {
  async add (context, contactData: ContactState) {
    let url = processLocalizedURLAddress(config.bocm.contactUs.endpoint)
    try {
      return await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(contactData)
      }).then(res => res.json()).then(res => res)
    } catch (e) {
      rootStore.dispatch('notification/spawnNotification', {
        type: 'error',
        message: i18n.t('Something went wrong. Try again in a few seconds.'),
        action1: { label: i18n.t('OK') }
      })
    };
  }
}

export default actions
